<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://use.typekit.net/cup5lij.css" rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="dist/css/bootstrap.css" type="text/css" />

	<!--Fontawesome CDN -->
	<script src="https://use.fontawesome.com/f54c94936a.js"></script>

	<title>Strategic Plan 2018-23 | SUNY Morrisville</title>
	<meta name="description" content="SUNY Morrisville is a model of innovative applied education &mdash; a place where students begin crafting exciting careers through real-world experiences. Our action-oriented learning labs allow students to “get their hands dirty” and engage in ways that go beyond the traditional classroom environment." />
	<meta name="abstract" content="SUNY Morrisville is a model of innovative applied education &mdash; a place where students begin crafting exciting careers through real-world experiences. Our action-oriented learning labs allow students to “get their hands dirty” and engage in ways that go beyond the traditional classroom environment." />
	<meta name="keywords" content="Program, Degree, College, University, Agriculture, Automotive, Business, Communications, Computer Science, Construction and Building, Engineering Technology, Equine, Health, Hospitality, Natural Resources, Nursing, Horticulture, College, University, Trans" />

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-BXHB1X1XYC"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-BXHB1X1XYC');
	</script>
</head>
<body>
	<nav id="secondary-nav" class="navbar navbar-expand-lg">
		<div class="container">
			<div class="navbar-brand social-icons text-left">
				<!-- #include file= "incs/social-icons.aspx" -->	
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<i class="navbar-toggler-icon fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse flex-grow-1 text-lg-right" id="navbarNav">
				<ul class="navbar-nav ml-auto flex-nowrap">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://www.morrisville.edu/about" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About <i class="fa fa-chevron-down"></i></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<div class="dropdown-item"><a href="http://morrisville.edu/campus-maps">Campus Maps</a></div>
							<div class="dropdown-item"><a href="http://morrisville.edu/facilities">Facilities</a></div>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" target="_blank" href="http://athletics.morrisville.edu">Athletics</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="http://www.morrisville.edu/giving">Give</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contact <i class="fa fa-chevron-down"></i></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink2">
							<div class="dropdown-item"><a href="http://morrisville.edu/directory">Directory</a></div>
							<div class="dropdown-item"><a href="http://morrisville.edu/offices">Offices</a></div>							
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="jumbotron jumbotron-fluid">
		<div class="container">			
			<div class="title">
				<div class="brand mb-4">
					<img class="img-fluid" src="img/logo.png" alt="SUNY Morrisville" />
				</div>
				<h1 class="page-title">Strategic Plan</h1>
				<p class="page-year">2018&ndash;23</p>
			</div>
		</div>
	</div>
	<div class="buffer">
		<div class="container">
			<div class="text-sm-right">
				<a class="btn btn-special" href="docs/strategic-plan-combined.pdf"><i class="fa fa-file-pdf-o"></i>&ensp;Download PDF</a>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="container">
			<div class="row mb-4">
				<div class="col-md-3 text-center">
					<img class="rounded-circle img-fluid" src="img/pres-rogers.png" alt="David E. Rogers, President" />
					<p class="small mt-4">David E. Rogers, Ph.D.<br /> <em>President</em></p>
				</div>
				<div class="col-md-9 pullquote">
					<p class="accent">
						We are committed to the success of our students.
					</p>
					<p>
						I am pleased to present this updated version of Morrisville's Strategic Plan, representing the 2018-2023 period.
					</p>
					<p>
						We have done great work together this past year, and you will find evidence of significant accomplishment with this year's update. 
					</p>
					<p>
						We have recently improved our procedures to both gather and analyze information attendant to evaluating our overall success in mission outcomes. These efforts help to inform improvements in both programs and policies. This is a sign of a maturing organization that is better prepared to meet challenges that lie ahead. 
					</p>
					<div class="accordion welcome" id="accordion">
						<div id="more" class="collapse" aria-labelledby="heading-more" data-parent="#accordion">
							<div>
								<p>
									We are committed to the success of our students. Not only do we work hard to create a welcoming and affirming culture, but we strive to create supportive connections for all students to succeed. And when they encounter difficulties&mdash;and even before those difficulties arise&mdash;we want to anticipate how to overcome those challenges.
								</p>
								<p>
									Morrisville is an institution with grit and determination. We meet the challenges of our time, while also individualizing education for students who have their sights on transforming the future. Our students, faculty and staff are unrelenting in their entrepreneurial pursuit of making a positive difference throughout New York communities. They form valuable partnerships that weave and reinforce the fabric of our economy and society.
								</p>
								<p>
									Through challenges great and small, Morrisville's commitment to its mission has led both to adaptation and transformation as we chart a sustainable pathway for our future. The recent accomplishments and future priorities outlined in this updated document serve as testament to the success of Morrisville's mission as a catalyst for both student development and community growth.
								</p>
							</div>
						</div>
						<div id="heading-more">
							<h4 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#more" aria-expanded="true" aria-controls="more">
									<span class="sr-only">Read More</span>
								</button>
							</h4>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="d-flex align-items-stretch col-lg-6 my-4">
					<div class="card-alt card">
						<div class="card-header">
							<h2 class="card-title mb-0">Vision</h2>
						</div>
						<div class="card-body">
							<p class="lead">
								SUNY Morrisville aspires to be a recognized leader in innovative applied education.
							</p>
						</div>
						<div class="card-footer">

						</div>
					</div>
				</div>
				<div class="d-flex align-items-stretch col-lg-6 my-4">
					<div class="card-alt card">
						<div class="card-header">
							<h2 class="card-title mb-0">Mission</h2>
						</div>
						<div class="card-body">
							<p class="lead">
								SUNY Morrisville works to offer diverse learning experiences so that graduates may pursue rewarding lives and careers, become engaged citizens, and contribute to our collective future.
							</p>
						</div>
						<div class="card-footer">

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="accordion ilo mb-4" id="ilo-accordion">
						<div class="card-alt card">
							<div class="card-header collapsed" id="ilo-heading" data-toggle="collapse" data-target="#ilo-body" aria-expanded="true" aria-controls="ilo-body">
								<h2 class="mb-0">
									<button class="btn btn-link" type="button">
										Institutional Learning Outcomes
									</button>
								</h2>
							</div>
							<div class="card-body text-left">
								<p>
									The SUNY Morrisville Institutional Learning Outcomes (ILOs) were developed through a Provost ad-hoc workgroup and approved by shared governance and leadership in January 2019. These outcomes represent the broad SUNY Morrisville curricular and co-curricular student experiences upon attending and graduating from SUNY Morrisville.
								</p>

								<div id="ilo-body" class="collapse" aria-labelledby="ilo-heading" data-parent="#ilo-accordion">
									<p>
										Upon successful completion SUNY Morrisville students should be able to:
									</p>
									<h3>Institutional Learning Outcome 1</h3>
									<p>
										Demonstrate professional readiness through service, leadership, innovative technology, and structured reflection in applied learning and in various campus life venues.
									</p>
									<h3>Institutional Learning Outcome 2</h3>
									<p>
										Use communication skills in composing written texts that effectively inform or persuade, and to engage in discussion, debate, and public speaking in a manner suitable to the listener(s) and the discourse.
									</p>
									<h3>Institutional Learning Outcome 3</h3>
									<p>
										Employ critical thinking and creativity in a variety of learning, experiential, or community contexts.
									</p>
									<h3>Institutional Learning Outcome 4</h3>
									<p>
										Practice quantitative and scientific reasoning in several learning settings.
									</p>
									<h3>Institutional Learning Outcome 5</h3>
									<p>
										Apply information literacy abilities to effectively discover information, to understand how information is produced and valued and to use information ethically in communities of practice.
									</p>
									<h3>Institutional Learning Outcome 6</h3>
									<p>
										Engage in communication and collaboration across diverse communities and organizations and reflect on the experience.
									</p>
									<h3>Institutional Learning Outcome 7</h3>
									<p>
										Recognize, adapt, or apply sustainable and ethical practices within social, economic, and environmental spheres in multiple settings.
									</p>
								</div>
							</div>
							<div class="card-footer">

							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix mt-4"></div>
			<div class="container">
				<ul class="nav nav-tabs" id="Tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="pillar-01-tab" data-toggle="tab" href="#pillar-01" role="tab" aria-controls="pillar-01" aria-selected="true">Inspire Learning Through Experience</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pillar-02-tab" data-toggle="tab" href="#pillar-02" role="tab" aria-controls="pillar-02" aria-selected="false">Build Community</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pillar-03-tab" data-toggle="tab" href="#pillar-03" role="tab" aria-controls="pillar-03" aria-selected="false">Achieve a Sustainable Future</a>
					</li>
				</ul>
				<div class="tab-content" id="TabsContent">
					<!-- #include file= "incs/pillar1.aspx" -->					
					<!-- #include file= "incs/pillar2.aspx" -->
					<!-- #include file= "incs/pillar3.aspx" -->
				</div>
			</div>
		</div>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<a href="http://morrisville.edu"><img class="img-fluid mb-4" src="img/logo.png" alt="SUNY Morrisville" /></a>
						<h2 class="footer-title"><a href="http://morrisville.edu">SUNY Morrisville</a></h2>
						<p>
							P.O. Box 901<br />
							80 Eaton St.<br />
							Morrisville, NY 13408<br />
							1.315.684.6000
						</p>
						<img class="img-fluid mt-4" src="img/suny-logo.png" alt="The State University of New York" />
					</div>
					<div class="col-md-9">
						<h2 class="quick-links">Quick Links</h2>
						<ul class="row list-unstyled">
							<li class="col-md-6"><a href="http://morrisville.edu/apply">Apply</a></li>
							<li class="col-md-6"><a href="https://www.morrisville.edu/contact/offices/technology-services/blackboard">Blackboard</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/campus-maps">Campus Maps</a></li>
							<li class="col-md-6"><a href="https://www.morrisville.edu/contact/offices/campus-store">Campus Store</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/cancellations">Cancellations</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/emergency-information">Emergency Information</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/careers">Employment Opportunities</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/events-list">Events</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/library">Library</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/web-faculty">Web for Faculty</a></li>
							<li class="col-md-6"><a href="http://morrisville.edu/web-students">Web for Students</a></li>
							<li class="col-md-6"><a href="http://webmail.morrisville.edu/">Webmail</a></li>
						</ul>
					</div>
				</div>
				<div class="social-icons text-center mt-4">
					<!-- #include file= "incs/social-icons.aspx" -->	
				</div>
				<div class="subfooter text-center">
					<p>&copy; 2018 SUNY Morrisville. All Rights Reserved. <a href="http://morrisville.edu/privacy-policy/">Privacy Policy</a>. <a href="http://morrisville.edu/accessibility-policy/">Accessibility</a>.</p>
				</div>
			</div>
		</footer>

		<!-- Optional JavaScript -->

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="dist/js/bootstrap.min.js" type="text/javascript"></script>
	</body>
	</html>