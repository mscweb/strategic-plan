<h2>To promote inquiry and scholarship at all levels</h2>

<!-- <p>
	In this area, we should possibly define inquiry and scholarship.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>2.1</b>
			</td>
			<td>
				Encourage and support opportunities to present, attend and network at campus, local, regional, national and global professional conferences, meetings and trainings
			</td>
		</tr>
		<tr>
			<td>
				<b>2.2</b>
			</td>
			<td>
				Encourage academic research-based activity by providing comprehensive support for internal and external research
			</td>
		</tr>
		<tr>
			<td>
				<b>2.3</b>
			</td>
			<td>
				Provide opportunities for student participation in research working with or under the guidance of faculty
			</td>
		</tr>
		<tr>
			<td>
				<b>2.4</b>
			</td>
			<td>
				Continually assess student learning at the institutional, unit, program and course level and useresults to implement improvements
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<ul class="list-group priorities mb-4">
	<li class="list-group-item">MAC will assist with upgrading the dairy plant to better the academic mission and allow for additional production of dairy items &ndash; Fall 2020 completion</li>
	<li class="list-group-item">Offer funding to faculty and staff for student applied learning projects in fall 2018</li>
	<li class="list-group-item">Develop a Teaching & Learning Center working with the Professional Development Committee to expand training</li>
	<li class="list-group-item">Explore forming an Interdisciplinary Research Group (IRG)</li>
	<li class="list-group-item">Continue to support HILT training for faculty and staff</li>
	<li class="list-group-item">Support funding for faculty liaison for each school and the Norwich Campus for fall 2018 and spring 2019 to inventory, expand, and assess student applied learning activities</li>
</ul>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Submitted Grant Proposals totaling $1.08 million</h4>
			<p class="card-text">Received $723,195 in awards to date</p>
		</div>
	</div>
	<div class="card featured">
		<div class="card-body">
			<h4 class="card-title">Ranked #1 Top Brewing School in North America</h4>
			<p class="card-text">U.S. Open College Beer Championships</p>			
		</div>
		<img class="card-img-bottom" src="img/accomplishments/brewing-03.png" alt="head brewer michael coons brews beer">
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">$1.2 million award from REDC</h4>
			<p class="card-text">CNY Region Award for CEA/Four Seasons Farm</p>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Student presentations at the 26<sup>th</sup> Annual CSTEP Statewide Student Conference</h4>
			<ul class="list-group list-group-flush">
				<li class="list-group-item"><em>Heath Issues Related to the Overuse of Triclosan</em></li>
				<li class="list-group-item"><em>Lyme Disease in Dogs</em></li>
				<li class="list-group-item"><em>Deoxyribonucleic Acid (DNS) as a Forensic Science Tool</em></li>
			</ul>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">SUNY Student Undergraduate Research Conference presentations</h4>
			<ul class="list-group list-group-flush">
				<li class="list-group-item"><em>Taurine: How Much is Too Much? Examining the Mechanism Behind the Stimulant Effects of Taurine</em></li>
				<li class="list-group-item"><em>Korean Feminism: Unresolved Gender Issues Dragging Korea's Societal Evolution (Sickening Privilege: Walls Piled Around Women in South Korea)</em></li>
			</ul>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Eight students presented at the Phi Beta Lambda NYS Leadership Conference
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">USDA's Distributed On-Farm Bioenergy, Biofuels and Biochemicals (FarmBio3) Development and Production via Integrated Catalytic Thermolysis</h4>
			<p class="card-text">
				2017 final reporting; one technical publication
			</p>
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>
	</div>
	<div class="card">		
		<div class="card-body">
			<h4 class="card-title">HeatSmart CNY</h4>
			<p class="card-text">
				Collaboration with CNY Regional Planning and Development on NYSERDA grant <em>HeatSmart CNY</em> a geothermal program with an educational component
			</p>
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
	<div class="card">
		<img class="card-img-top" src="img/accomplishments/hemp-01.png" alt="industrial hemp field">
		<div class="card-body">
			<h4 class="card-title">Industrial Hemp Research</h4>
			<p class="card-text">
				Empire State Development and NYS Department of Agriculture & Markets are partnering with SUNY Morrisville to support industrial hemp research
			</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Mastering Accountability</h4>
			<p class="card-text">
				2018 Women in Agriculture Leadership Conference, Minneapolis, MN
			</p>
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/agriculture-01.png" alt="tractor in corn field">
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Agriculture Economics and Policy in the U.S.</h4>
			<p class="card-text">
				Presented to leaders from Bosnia/Herzengovina, Bulgaria, Czech Republic, Hungary, Netherlands, Poland, Serbia and United Kingdom
			</p>
			
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Dairy Fly Control</h4>
			<p class="card-text">
				ASAS Pre-conference, Fort Worth, TX
			</p>
			
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">The Video Game Book Club; A New Spin on an Old Tradition</h4> 
			<p class="card-text">
				2017 SUNY Librarians Association Conference and Journey Into Literacy Conference
			</p>
			
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Shouldn't This Be at a Public Library? A Collection to Support the Community, Not the Curriculum</h4>
			<p class="card-text">
				2018 SUNY Librarian Association Conference
			</p>
		</div>
		<div class="card-footer">
			<p>Faculty Research and Presentations</p>
		</div>	
	</div>
</div>