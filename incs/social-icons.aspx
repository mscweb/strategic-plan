<ul class="list-unstyled list-inline">
	<li class="list-inline-item"><a href="https://www.facebook.com/sunymorrisville"><i class="fa fa-facebook"><span class="sr-only">Facebook</span></i></a></li>
	<li class="list-inline-item"><a href="https://twitter.com/SUNYmorrisville"><i class="fa fa-twitter"><span class="sr-only">Twitter</span></i></a></li>
	<li class="list-inline-item"><a href="https://www.youtube.com/user/morrisvillestate"><i class="fa fa-youtube-play"><span class="sr-only">YouTube</span></i></a></li>
	<li class="list-inline-item"><a href="https://instagram.com/sunymorrisville/"><i class="fa fa-instagram"><span class="sr-only">Instagram</span></i></a></li>
	<li class="list-inline-item"><a href="https://www.linkedin.com/company/morrisville-state-college"><i class="fa fa-linkedin"><span class="sr-only">LinkedIn</span></i></a></li>
</ul>