<h2>To assess and document success in achieving the College's mission</h2>

<!-- <p>
	In this area, we should possibly define the college's mission.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>9.1</b>
			</td>
			<td>
				Embrace shared governance to promote open information sharing and participatory decision-making
			</td>
		</tr>
		<tr>
			<td>
				<b>9.2</b>
			</td>
			<td>
				Reinforce the cycle of college-wide planning, budgeting, allocation and assessment
			</td>
		</tr>
		<tr>
			<td>
				<b>9.3</b>
			</td>
			<td>
				Adopt institutional dashboard indicators (key performance indicators)
			</td>
		</tr>
		<tr>
			<td>
				<b>9.4</b>
			</td>
			<td>
				Implement strategic information collection in support of institutional decision-making
			</td>
		</tr>
		<tr>
			<td>
				<b>9.5</b>
			</td>
			<td>
				Continually assess student learning at the institutional, unit, program and course level, and use results to implement improvements
			</td>
		</tr>
		<tr>
			<td>
				<b>9.6</b>
			</td>
			<td>
				Continually assess institutional mission and effectiveness
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<ul class="list-group priorities">
	<li class="list-group-item">Form the SUNY Morrisville Assessment Council (SMAC)</li>
	<li class="list-group-item">Review and adopt the Institutional Learning Outcomes working with the SUNY Morrisville Assessment Council (SMAC) and College Senate</li>
	<li class="list-group-item">Develop a College-wide Assessment Plan for academic areas and non-academic support areas</li>
	<li class="list-group-item">Complete a Facilities Master Plan</li>
	<li class="list-group-item">Complete a Strategic Enrollment Plan (SEM) Plan</li>
	<li class="list-group-item">Form an Enrollment Council</li>
	<li class="list-group-item">Enhance the outcomes assessment process with data collection, reporting, and sharing the use of the results for change</li>
	<li class="list-group-item">Continue to improve the budget initiative process through the Strategic Planning & Budgeting Committee (SPBC)</li>
	<li class="list-group-item">Complete annual refresh of the Strategic Plan</li>
	<li class="list-group-item">Develop an Operational Plan linked to functional plans (SEM, Facilities Master Plan, etc).</li>
	<li class="list-group-item">Expand Student Affairs division assessment map or dashboard of KPIs</li>
	<li class="list-group-item">Review new ways to assess the student population to meet students and college needs</li>
</ul>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Conducted semiannual Shared Governance Workshops with College Senate and Administration
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Established Office of Assessment, Planning and Educational Development
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Middle States Commission on Higher Education</h4>
			<p class="card-text">
				<ul>
					<li>Last reaffirmation 2017</li>
					<li>Next Self-Study Evaluation: 2012-2022</li>
					<li>Next Mid-Point Peer Review: 2026</li>
				</ul>
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">
				Technology Accreditation Commission of the Accreditation Board for Engineering and Technology (TAC ABET)
			</h4>
		</div>	
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/auto.png" alt="working with auto parts">
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">
				National Automotive Technicians Education Foundation
			</h4>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Academy of Nutrition and Dietetics, Accreditation Council for Education in Nutrition and Dietetics</h4>
			<p class="card-text">
				<ul>
					<li>Last Accreditation 2009 (expires 2019)</li>
					<li>Performance and accountability report (PAR) report - passed October, 2014.</li>
					<li>Self-study submitted - site visit scheduled for October 2018 with final decision July 2019</li>
				</ul>
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Accreditation Commission for Education in Nursing</h4>
			<p class="card-text">
				<ul>
					<li>Most recent commission action &mdash; July 2013</li>
					<li>Upcoming site visit - September 2018</li>
				</ul>
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">
				The Accreditation Council for Business Schools and Programs
			</h4>
			<p class="card-text">
				Bachelor's and associate degrees in Business Administration, Accounting, Office Administration, Office Management, Medical Office Administration and Information processing
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Society of American Foresters (SAF)</h4>
			<p class="card-text">
				The educational program in the Natural Resources Conservation A.A.S., Forest Technology is accredited through 2027.
			</p>
		</div>	
	</div>
</div>