<h2>To engage the local community in civic and cultural affairs</h2>

<!-- <p>
	In this area, we should possibly define our vision for this engagement.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>5.1</b>
			</td>
			<td>
				Cultivate a campus environment that respects and celebrates diversity of individual backgrounds, circumstances and thought
			</td>
		</tr>
		<tr>
			<td>
				<b>5.2</b>
			</td>
			<td>
				Increase the number of professional conferences, campus and community hosted events
			</td>
		</tr>
		<tr>
			<td>
				<b>5.3</b>
			</td>
			<td>
				Align campus services and activities to support a safe and vibrant community of learners
			</td>
		</tr>
		<tr>
			<td>
				<b>5.4</b>
			</td>
			<td>
				Expand interaction between the college and surrounding communities
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<ul class="list-group priorities">
	<li class="list-group-item">(MAC) Develop late night meal program for residential students &mdash; completion Fall 2019</li>
	<li class="list-group-item">Host an Ag Summit during Fall 2018 to promote regional partnerships, workforce opportunities, and academic degree programs</li>
	<li class="list-group-item">Host an Applied Learning Conference in spring 2019 to showcase student work under the SUNY System Applied Learning Dimensions</li>
	<li class="list-group-item">MOVE-growing grant process for community service to collaborate with other clubs, organizations, faculty, and staff</li>
	<li class="list-group-item">Participate in Residence Life bulletin board series regarding educational information and outreach programming related to Health and Counseling Services</li>
	<li class="list-group-item">Improve access to Counseling Services, which would ideally be accomplished by hiring an additional LMHC, reevaluate policy and procedures, and scheduling process to ensure maximum efficiency is being achieved</li>
</ul>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card featured">
		<img class="card-img-top" src="img/accomplishments/trustees.png" alt="SUNY Board of Trustees meeting">
		<div class="card-body">
			<h4 class="card-title">SUNY Board of Trustees</h4>
			<p class="card-text">
				Hosted May 2017 meeting
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Student Activities supported more than 50 activities and logged over 3200 hours of community service
			</p>
		</div>
		<div class="card-footer">
			<p>Community Service</p>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				The Athletic Department participated in a record number of community service projects (3,000 hours) and raised $7,000. Several of the initiatives this year tied directly to the Morrisville Eaton community and school system. Leading the way was the "Mondays with the Mustangs" program where teams engaged MECS elementary School students during recess hours.
			</p>
		</div>
		<div class="card-footer">
			<p>Community Service</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Chenango County Department of Social Service Commissioner Bette Osborne was a guest speaker on Child Welfare Services
			</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Provided high-quality services under high demand, which included 2,997 appointments for health services, 1,899 appointments for counseling services. Campus and community outreach programming provided to 1,479 individuals, including two Safe-TALK (suicide prevention) events, Depression Screening Day
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Maintain strong relationships with Madison County Health Dept., NYS Department of Health (DOH), Help Restore Hope, Mohawk Valley Immunization Alliance, NYS Opioid Overdose Program, and Madison County Mobile Crisis Unit
			</p>
		</div>	
	</div>
</div>