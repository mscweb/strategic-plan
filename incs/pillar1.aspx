<div class="tab-pane fade show active mt-4" id="pillar-01" role="tabpanel" aria-labelledby="pillar-01-tab">
	<div class="row d-flex flex-row-reverse">
		<div class="col-lg-3 mb-4">
			<div class="nav flex-column nav-pills sticky-top" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-goal-01-tab" data-toggle="pill" href="#v-pills-goal-01" role="tab" aria-controls="v-pills-goal-01" aria-selected="true">
					<b>Goal 1:</b> To offer career-focused, experiential learning
				</a>
				<a class="nav-link" id="v-pills-goal-02-tab" data-toggle="pill" href="#v-pills-goal-02" role="tab" aria-controls="v-pills-goal-02" aria-selected="false">
					<b>Goal 2:</b> To promote inquiry and scholarship at all levels
				</a>
				<a class="nav-link" id="v-pills-goal-03-tab" data-toggle="pill" href="#v-pills-goal-03" role="tab" aria-controls="v-pills-goal-03" aria-selected="false">
					<b>Goal 3:</b> To enhance cultural competency and promote equity and inclusion
				</a>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-goal-01" role="tabpanel" aria-labelledby="v-pills-goal-01-tab">
					<!-- #include file= "pillar1-goal1.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-02" role="tabpanel" aria-labelledby="v-pills-goal-02-tab">
					<!-- #include file= "pillar1-goal2.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-03" role="tabpanel" aria-labelledby="v-pills-goal-03-tab">
					<!-- #include file= "pillar1-goal3.aspx" -->
				</div>
			</div>
		</div>
	</div>
</div>