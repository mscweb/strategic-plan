<h2>To develop campus resources and operations with minimum resource footprint</h2>

<!-- <p>
	In this area, we should possibly define "minimum resource footprint.""
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>7.1</b>
			</td>
			<td>
				Enhance and maintain a physical infrastructure that is safe, accessible, green and aesthetically appealing
			</td>
		</tr>
		<tr>
			<td>
				<b>7.2</b>
			</td>
			<td>
				Utilize a transparent budget process which incorporates short-term and long-term financial strategies
			</td>
		</tr>
		<tr>
			<td>
				<b>7.3</b>
			</td>
			<td>
				Commit to institutional practices which optimize sustainable practices
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>

<div class="accordion mb-4" id="accordion">
	<div class="card">
		<div class="card-header collapsed" id="heading-mac-02" data-toggle="collapse" data-target="#collapse-mac-02" aria-expanded="true" aria-controls="collapse-mac-02">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Morrisville Auxiliary Corporation
				</button>
			</h4>
		</div>
		<div id="collapse-mac-02" class="collapse" aria-labelledby="heading-mac-02" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">MAC will upgrade infrastructure, equipment and facilities within the equine program completion Spring 2020</li>					
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-budget" data-toggle="collapse" data-target="#collapse-budget" aria-expanded="true" aria-controls="collapse-budget">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Budget and Training
				</button>
			</h4>
		</div>
		<div id="collapse-budget" class="collapse" aria-labelledby="heading-budget" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Obtain and implement a budget system which will facilitate the creation and use of the college budget</li>
					<li class="list-group-item">Provide training for fund custodians to ensure familiarity with SUNY Business Intelligence and with the budget process</li>
					<li class="list-group-item">Provide training on purchasing guidelines and process</li>
					<li class="list-group-item">Provide training for completion of travel claims</li>					
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-finsvcs-02" data-toggle="collapse" data-target="#collapse-finsvcs-02" aria-expanded="true" aria-controls="collapse-finsvcs-02">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Student Accounts
				</button>
			</h4>
		</div>
		<div id="collapse-finsvcs-02" class="collapse" aria-labelledby="heading-finsvcs-02" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Implement Ecommerce Services, which will streamline peripheral cash locations working toward PCI Compliance Regulations and expectations.</li>
					<li class="list-group-item">Implement Flywire or Pay My Tuition Services, which will provide convenience for international students and families to make their tuition payments without the complication of monetary conversion rates.</li>
					<li class="list-group-item">Online Enrollment Confirmation. Work with the Technology Department to offer students an easier way to complete registration requirements and be able to accept charges confirm enrollment, submit an insurance waiver and complete their Federal Aid Authorizatio, all in one place. This will be more convenient for our customers and also reduce the bottleneck the current process creates for the campus.</li>
					<li class="list-group-item">
						<p>Student Health Insurance Waiver will be changed from a soft waiver to a hard waiver</p>
						<p>Students will have their waivers reviewed by HF&C for comparable coverage and will be the point of contact for inquiries</p>
						<p>Students will be able to upload a copy of their insurance card when completing the waiver</p>
						<p>This new process will ensure our students have adequate coverage and also allows the Health Center to access the uploaded images as needed for submitting claims</p>
					</li>				
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-reslife" data-toggle="collapse" data-target="#collapse-reslife" aria-expanded="true" aria-controls="collapse-reslife">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Residence Life
				</button>
			</h4>
		</div>
		<div id="collapse-reslife" class="collapse" aria-labelledby="heading-reslife" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Assessment of Residential Curriculum and Implementation of Lesson Plan Templates in Roompact.</li>					
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-facilities" data-toggle="collapse" data-target="#collapse-facilities" aria-expanded="true" aria-controls="collapse-facilities">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Facilities
				</button>
			</h4>
		</div>
		<div id="collapse-facilities" class="collapse" aria-labelledby="heading-facilities" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Complete a $30 million renovation of Syracuse EOC</li>	
					<li class="list-group-item">Continue the design and construction of the Agricultural Engineering Building</li>
					<li class="list-group-item">Redesign and renovate the Methane Digester</li>					
				</ul>
			</div>
		</div>
	</div>
</div>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card featured">
		<img class="card-img-top" src="img/accomplishments/solar-01.png" alt="installing solar array">
		<div class="card-body">
			<p class="card-text">
				The Norwich Campus worked with Renewable Energy faculty to install a small solar array at Follett Hall in fall 2017
			</p>
		</div>	
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/solar-02.png" alt="installing solar batteries">
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Awarded Tree Campus USA 2017 from the Arbor Day Foundation
			</p>
		</div>
		<img class="card-img-bottom" src="img/accomplishments/tree-campus.png" alt="tree campus planting">	
	</div>
</div>