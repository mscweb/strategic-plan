<div class="tab-pane fade show mt-4" id="pillar-03" role="tabpanel" aria-labelledby="pillar-03-tab">
	<div class="row d-flex flex-row-reverse">
		<div class="col-lg-3 mb-4">
			<div class="nav flex-column nav-pills sticky-top" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-goal-07-tab" data-toggle="pill" href="#v-pills-goal-07" role="tab" aria-controls="v-pills-goal-07" aria-selected="true">
					<b>Goal 7:</b> To develop campus resources and operations with minimum resource footprint
				</a>
				<a class="nav-link" id="v-pills-goal-08-tab" data-toggle="pill" href="#v-pills-goal-08" role="tab" aria-controls="v-pills-goal-08" aria-selected="false">
					<b>Goal 8:</b> To achieve effective and sustainable levels of required resources
				</a>
				<a class="nav-link" id="v-pills-goal-09-tab" data-toggle="pill" href="#v-pills-goal-09" role="tab" aria-controls="v-pills-goal-09" aria-selected="false">
					<b>Goal 9:</b> To assess and document success in achieving the College's mission
				</a>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-goal-07" role="tabpanel" aria-labelledby="v-pills-goal-07-tab">
					<!-- #include file= "pillar3-goal7.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-08" role="tabpanel" aria-labelledby="v-pills-goal-08-tab">
					<!-- #include file= "pillar3-goal8.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-09" role="tabpanel" aria-labelledby="v-pills-goal-09-tab">
					<!-- #include file= "pillar3-goal9.aspx" -->
				</div>
			</div>
		</div>
	</div>
</div>