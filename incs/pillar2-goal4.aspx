<h2>To create a vibrant campus community for personal interaction and growth</h2>

<!-- <p>
	In this area, we should possibly define our vision of a vibrant campus community.
</p>
 -->
<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>4.1</b>
			</td>
			<td>
				Promote greater academic and co-curricular engagement for students
			</td>
		</tr>
		<tr>
			<td>
				<b>4.2</b>
			</td>
			<td>
				Create more opportunities for students, faculty and staff to interact socially, academically and recreationally
			</td>
		</tr>
		<tr>
			<td>
				<b>4.3</b>
			</td>
			<td>
				Enhance student awareness of and participation in programs promoting success
			</td>
		</tr>
		<tr>
			<td>
				<b>4.4</b>
			</td>
			<td>
				Celebrate and communicate campus achievements through multiple media outlets
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>

<div class="accordion mb-4" id="accordion">
	<div class="card">
		<div class="card-header collapsed" id="heading-mac-01" data-toggle="collapse" data-target="#collapse-mac-01" aria-expanded="true" aria-controls="collapse-mac-01">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Morrisville Auxiliary Corporation
				</button>
			</h4>
		</div>
		<div id="collapse-mac-01" class="collapse" aria-labelledby="heading-mac-01" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Support professional design and construction process for the Norwich Children &amp; Family Center</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-finsvcs-01" data-toggle="collapse" data-target="#collapse-finsvcs-01" aria-expanded="true" aria-controls="collapse-finsvcs-01">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Financial Aid and Student Accounts
				</button>
			</h4>
		</div>
		<div id="collapse-finsvcs-01" class="collapse" aria-labelledby="heading-finsvcs-01" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Financial Aid: Enhance the process for completing federal verification</li>
					<li class="list-group-item">Financial Aid and Student Accounts: Foster the development of a "Financial Services Center" that will provide a range of services, including financial aid advisement, payment plan advisement, financial literacy, scholarship search and outreach</li>
					<li class="list-group-item">Financial Aid and Student Accounts: Create three student "Financial Peer Advisor" positions, which will be part of the "Financial Service Center" initiative</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-advise-01" data-toggle="collapse" data-target="#collapse-advise-01" aria-expanded="true" aria-controls="collapse-advise-01">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Campus-wide Advisement
				</button>
			</h4>
		</div>
		<div id="collapse-advise-01" class="collapse" aria-labelledby="heading-advise-01" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Enhance Orientation Experience for incoming first-year students.</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-commmark" data-toggle="collapse" data-target="#collapse-commmark" aria-expanded="true" aria-controls="collapse-commmark">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Communications &amp; Marketing
				</button>
			</h4>
		</div>
		<div id="collapse-commmark" class="collapse" aria-labelledby="heading-commmark" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Create program-group-level and program-level communications and marketing plans</li>
					<li class="list-group-item">Collaborate with faculty and staff within the 10 program group areas, as well as the individual programs, to interpret college brand and messaging on a more localized level</li>
					<li class="list-group-item">Develop a specialized advertising strategy</li>
					<li class="list-group-item">Develop a unified, localized branding system for the College's offices, departments and programs to be used in publications, stationary and other applications</li>
					<li class="list-group-item">Make online advertising more segmented, targeted and granular, based on program-level marketing</li>
					<li class="list-group-item">Rollout the new branding and messaging further, from digital presence and printed publications to promotional items and signage</li>
					<li class="list-group-item">Increase and measure awareness of SUNY Morrisville and its programs through local market research</li>
					<li class="list-group-item">Increase engagement with Morrisville, Norwich and other neighboring communities through local media and other opportunities</li>
					<li class="list-group-item">Rollout Content Management System (CMS) and training to empower faculty and staff to make updates to website, reviewed and published by Web Services</li>
					<li class="list-group-item">Improved campus communications, including revisions to the All-Campus Email Digest and exploration of additional "Morrisville in the News" newsletter and podcast</li>
					<li class="list-group-item">Expand video catalog to include more co-curricular activities and campus life</li>
					<li class="list-group-item">Explore additional social media video presence through Facebook Live, Instagram Stories and Snapchat</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-training" data-toggle="collapse" data-target="#collapse-training" aria-expanded="true" aria-controls="collapse-training">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Training
				</button>
			</h4>
		</div>
		<div id="collapse-training" class="collapse" aria-labelledby="heading-training" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Update college judicial materials and trainings to comport with first amendment guidance from SUNY Counsel</li>
					<li class="list-group-item">Train on Maxient software input, audit, reporting capabilities</li>
					<li class="list-group-item">Train SUNY Morrisville colleagues to host One Love Escalation Workshops on campus</li>
					<li class="list-group-item">Implement SUNY Judicial Institute project and annual trainings for my staff who assist with T9 cases</li>
					<li class="list-group-item">Soft launch of residential curriculum, Fall 2018</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">NEAC awards</h4>
			<p class="card-text">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">1<sup>st</sup> &ndash; Sportsmanship</li>
					<li class="list-group-item">2<sup>nd</sup> &ndash; Winning Percentage</li>
					<li class="list-group-item">3<sup>rd</sup> &ndash; Community Service</li>
					<li class="list-group-item">3<sup>rd</sup> &ndash; Overall President's Cup Finish</li>
				</ul>
			</p>
		</div>
		<div class="card-footer">
			<p>Athletics</p>
		</div>	
	</div>
	<div class="card featured">
		<div class="card-body">
			<h4 class="card-title">
				Formed Women's Rights and Concerns Committee
			</h4>
		</div>
		<div class="card-footer">
			<p>UUP</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Received a two year grant to participate in the Culture of Respect program through NASPA
			</p>
		</div>
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/more-students.png" alt="happy students">
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Created and implemented Bias Acts Response Team with online reporting option
			</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Chenango Arts Council</h4>
			<p class="card-text">
				Assisted with the Chenango Arts Council fall 2017 Arts Certificate Program including a film documentary, exhibit, and live theater
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Veteran's Advocate position increased programming and services provided to veterans on campus
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Charter Member of the New York Pennsylvania Campus Compact (CCNYPA)
			</p>
		</div>	
	</div>
</div>