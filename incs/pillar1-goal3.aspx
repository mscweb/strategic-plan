<h2>To enhance cultural competency and promote equity and inclusion</h2>

<!-- <p>
	In this area, we should possibly define cultural competency.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>3.1</b>
			</td>
			<td>
				Implement innovative recruitment strategies to enhance campus diversity in all areas
			</td>
		</tr>
		<tr>
			<td>
				<b>3.2</b>
			</td>
			<td>
				Leverage the significant diversity of the campus community to strengthen perspectives on individual and group similarities and differences
			</td>
		</tr>
		<tr>
			<td>
				<b>3.3</b>
			</td>
			<td>
				Utilize a multitude of cultural stories to enhance the curriculum across academic disciplines
			</td>
		</tr>
		<tr>
			<td>
				<b>3.4</b>
			</td>
			<td>
				Expand campus-based activities and training to enhance inclusiveness and equity
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<ul class="list-group priorities mb-4">
	<li class="list-group-item">
		Use collected data from the Climate Survey to guide the work of the Office of Diversity, Equity & Inclusivity. A proposal process for funds through the Sheila Johnson Foundation will be established to address issues presented in the data.
	</li>
	<li class="list-group-item">
		Have 100 percent of participants on search committees attend the workshop on Best Practices for Hiring, presented through the Office of Human Resources.
	</li>
	<li class="list-group-item">
		Engage the campus in conversations with Dr. Sam Museus on "Creating Culturally Engaged Communities" on October 19, 2018. A SUNY grant through the Office of Diversity, Equity & Inclusivity and funds through the NY6 grant will fund this event.
	</li>
	<li class="list-group-item">
		Build relationships with Clear Path for Veterans.
	</li>
	<li class="list-group-item">
		Use Bias Team to develop proactive protocols and procedures for campus crisis.
	</li>
	<li class="list-group-item">
		Continue to develop cultural programming through a "Building Beloved Community" series.
	</li>
	<li class="list-group-item">
		Task force for Equity in Salaries has made recommendations and funds have been set aside to address issues.
	</li>
	<li class="list-group-item">
		Investigate program in collaboration with Interfaith Works to develop a campuswide dialogue series.
	</li>
	<li class="list-group-item">
		Develop Sheila Johnson Leadership Program.
	</li>
	<li class="list-group-item">
		Continue education on web accessibility and universal design.
	</li>
</ul>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Award for Diversity, Inclusion, and Social Justice</h4>
			<p class="card-text">
				Dr. Timothy W. Gerken, associate professor of humanities, is the first recipient of the SUNY Office of Diversity, Equity and Inclusion (ODEI) Award for Diversity, Inclusion, and Social Justice.
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Whistling Vivaldi: How Stereotypes Affect Us and What We Can Do</h4>
			<p class="card-text">
				&mdash; Claude M. Steele
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Inclusive Excellence</h4>
			<p class="card-text">
				Dr. Damon Williams engaged the campus in dialogue on developing "Inclusive Excellence."
			</p>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Creating an Inclusive Campus Environment: Our Role and Responsibility as Team Members and Leaders</h4>
			<p class="card-text">
				&mdash; Dr. Kathy Obear
			</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Climate Survey</h4>
			<p class="card-text">
				Administration of the the National Institute for Transformation and Equity (NITE) climate survey to students, faculty and staff
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Building OUR Beloved Community</h4>
			<p class="card-text">
				Campus-wide programming
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Understanding the Borderlands: Situating Latinxs in the Contemporary Social and Political Landscape</h4>
			<p class="card-text">
				Discussed by Dr. Jorge Estrada 
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Trans 101 Workshop</h4>
			<p class="card-text">
				Introduced people to the issues transgender people face in our culture
			</p>
		</div>	
	</div>
	<div class="card featured">
		<img class="card-img-top" src="img/accomplishments/mlk-01.png" alt="Performers at Martin Luther King, Jr. Celebration Keynote">
		<div class="card-body">
			<h4 class="card-title">Martin Luther King, Jr. Celebration Keynote</h4>
			<p class="card-text">
				Mr. Max Alden Smith gave the keynote address for the Martin Luther King, Jr. Celebration discussing his heritage as a decedent of Harriet Russell who was a slave brought to Peterboro, NY by Ann and Gerrit Smith
			</p>
		</div>	
	</div>	
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Hosted SUNY Conversations in the Disciplines: Developing and Assessing Diversity Learning Outcomes
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				A two day opportunity was offered through Intergroup Dialogue for campus members to learn more about the program and participate in dialogue
			</p>
		</div>	
	</div>
</div>