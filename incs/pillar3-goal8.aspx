<h2>To achieve effective and sustainable levels of required resources</h2>

<!-- <p>
	In this area, we should possibly define required resources.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>8.1</b>
			</td>
			<td>
				Develop functional strategic plans that align with the College's comprehensive strategic plan
			</td>
		</tr>
		<tr>
			<td>
				<b>8.2</b>
			</td>
			<td>
				Maintain a breadth of program offerings that meet student and community needs
			</td>
		</tr>
		<tr>
			<td>
				<b>8.3</b>
			</td>
			<td>
				Review and adopt technologies that support teaching modalities to promote access and success
			</td>
		</tr>
		<tr>
			<td>
				<b>8.4</b>
			</td>
			<td>
				Recruit, retain and develop faculty and staff consistent with the needs of the campus community
			</td>
		</tr>
		<tr>
			<td>
				<b>8.5</b>
			</td>
			<td>
				Increase student enrollment, retention and completion
			</td>
		</tr>
		<tr>
			<td>
				<b>8.6</b>
			</td>
			<td>
				Engage external stakeholders to increase support from alumni, foundations and others with shared interests
			</td>
		</tr>
		<tr>
			<td>
				<b>8.7</b>
			</td>
			<td>
				Develop and implement campuswide Strategic Enrollment Plan
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>

<div class="accordion mb-4" id="accordion">
	<div class="card">
		<div class="card-header collapsed" id="heading-athletics" data-toggle="collapse" data-target="#collapse-athletics" aria-expanded="true" aria-controls="collapse-athletics">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Athletics
				</button>
			</h4>
		</div>
		<div id="collapse-athletics" class="collapse" aria-labelledby="heading-athletics" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">The Athletics Department will host Dan Tudor, an expert in the area of intercollegiate athletic recruiting. In addition to conducting a day long training program (including individual sessions by program), his firm will execute an in depth survey of all incoming freshman athletes regarding their selection process as well as their experience with SUNY Morrisville specifically in that regard.</li>
					<li class="list-group-item">The Athletics Department is looking for ways to apply a business model to the recruiting process which focuses on the customer service area as it relates directly to the recruiting experience. During the coming year we will be critically examining how we do campus visits, the communication process leading from prospect to recruit, to deposit. We will be looking to engage the larger campus community in our recruitment process.</li>					
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-pdc" data-toggle="collapse" data-target="#collapse-pdc" aria-expanded="true" aria-controls="collapse-pdc">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Program Development
				</button>
			</h4>
		</div>
		<div id="collapse-pdc" class="collapse" aria-labelledby="heading-pdc" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Form a Program Development Committee (PDC) to review feasibility of new academic degree/certificate programs and refresh existing</li>
					<li class="list-group-item">Respond to the Chancellor's call for expanded online course delivery by leveraging results of OpenSUNY Institutional Readiness process to increase online delivery of courses</li>
					<li class="list-group-item">Adjust program offerings to ensure relevance to both current and potential students </li>				
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-mac-03" data-toggle="collapse" data-target="#collapse-mac-03" aria-expanded="true" aria-controls="collapse-mac-03">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Morrisville Auxiliary Corporation
				</button>
			</h4>
		</div>
		<div id="collapse-mac-03" class="collapse" aria-labelledby="heading-mac-03" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">MAC to establish their own strategic plan &mdash; completion Spring 2019</li>			
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-adv" data-toggle="collapse" data-target="#collapse-adv" aria-expanded="true" aria-controls="collapse-adv">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Institutional Advancement
				</button>
			</h4>
		</div>
		<div id="collapse-adv" class="collapse" aria-labelledby="heading-adv" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Raise $500K cash in FY19: $100K unrestricted, $200K restricted, $200K endowed</li>
					<li class="list-group-item">Increase the number of alumni donors from 780 in FY18 to 900 in FY19</li>
					<li class="list-group-item">Promote the Seward Brooks Planned Giving society and secure 10 new planned gifts per year</li>
					<li class="list-group-item">Engage alumni, parents, friends and businesses in the life of the College both through their philanthropy and their mentorship and support for students, their education, and their careers</li>			
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-techsvcs" data-toggle="collapse" data-target="#collapse-techsvcs" aria-expanded="true" aria-controls="collapse-techsvcs">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Technology Services
				</button>
			</h4>
		</div>
		<div id="collapse-techsvcs" class="collapse" aria-labelledby="heading-techsvcs" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Upgrade to Banner 9</li>
					<li class="list-group-item">Upgrade Internet bandwidth to accommodate student, faculty and staff needs for Internet connectivity</li>
					<li class="list-group-item">Upgrade wireless and wired networks to accommodate increased number of devices used by students, faculty and staff</li>			
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-partner" data-toggle="collapse" data-target="#collapse-partner" aria-expanded="true" aria-controls="collapse-partner">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Partnership Office
				</button>
			</h4>
		</div>
		<div id="collapse-partner" class="collapse" aria-labelledby="heading-partner" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Develop website, promotional videos and marketing materials to further advertise the advantages of SUNY Morrisville's early college and college in the high school programs</li>
					<li class="list-group-item">Explore specific associate degree options with local high schools to further assist with bachelor's degree enrollment</li>		
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-admissions" data-toggle="collapse" data-target="#collapse-admissions" aria-expanded="true" aria-controls="collapse-admissions">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Admissions
				</button>
			</h4>
		</div>
		<div id="collapse-admissions" class="collapse" aria-labelledby="heading-admissions" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Host 2018 SUNY OpInform to engage with local guidance counselors and inform local partners of the many opportunities available at Morrisville</li>
					<li class="list-group-item">Further integrate social media into the admissions communication plan to increase yield and combat summer melt</li>
					<li class="list-group-item">Implement CRM and Document Imaging to increase efficiency of application review and recruitment efforts</li>
					<li class="list-group-item">Collaborate with the Office of Communications & Marketing to integrate new brand identity into all areas of admissions messaging</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-finsvcs-03" data-toggle="collapse" data-target="#collapse-finsvcs-03" aria-expanded="true" aria-controls="collapse-finsvcs-03">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Financial Service Center
				</button>
			</h4>
		</div>
		<div id="collapse-finsvcs-03" class="collapse" aria-labelledby="heading-finsvcs-03" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">From financial aid counseling to payment plans, the Financial Service Center will provide students with a one-stop-shop for all financial needs.</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-advise-03" data-toggle="collapse" data-target="#collapse-advise-03" aria-expanded="true" aria-controls="collapse-advise-03">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Campus-wide Advisement
				</button>
			</h4>
		</div>
		<div id="collapse-advise-03" class="collapse" aria-labelledby="heading-advise-03" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Starfish implementation: Will undergo faculty "alpha" user training Fall 2018 with an expected college-wide rollout Spring 2019. The Campuswide Advisement Office will lead early-warning intervention efforts.</li>
					<li class="list-group-item">Orientation and MAP: These two retention focused program areas, both coordinated by the Campuswide Advisement Office, will be reviewed for effectiveness and improved for 2019-2020</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">$2 million in philanthropic support</h4>
			<p class="card-text">
				Secured $2 million in philanthropic support in FY18, up from $1.73 in FY17
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">$413K in cash support</h4>
			<p class="card-text">
				Raised $413K in cash support for endowed and annually spendable programs
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">$179K in alumni Support</h4>
			<p class="card-text">
				Increased alumni support from $95K in FY17 to $179K in FY18, an increase of 88%
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Following the national trend of larger gifts from fewer donors, alumni donors decreased from 845 in FY17 to 814 in FY18
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Established an E-textbook program and Utilize Open Educational Resources to reduce the cost of books
			</p>
		</div>	
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/students.png" alt="students working">
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Dr. Jeffrey Docking, president of Adrian College and current chair of the NCAA Division III President's Council presented an Integration Institute Program on Sustainability and the Role of Athletics in Fulfilling That Mission
			</p>
		</div>	
	</div>
	<div class="card featured">
		<div class="card-body">
			<h4 class="card-title">Rebrand Initiative</h4>
			<p class="card-text">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Completed market research positioning and perception with Dartlet</li>
					<li class="list-group-item">Completed new messaging and creative design with Ologie</li>
					<li class="list-group-item">Completed website overhaul with Digital Echidna</li>
				</ul>
			</p>
		</div>
		<div class="card-footer">
			<p>Communications &amp; Marketing</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Momentum Magazine</h4>
			<p class="card-text">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Developed and carried out market research projects with partner Drive Research </li>
					<li class="list-group-item">Published two issues of <em>Momentum Magazine</em> </li>
					<li class="list-group-item">Expanded mailing to current students, faculty and staff</li>
					<li class="list-group-item">Won a 2018 SUNYCUAD Award for Excellence for the Fall 2017 issue</li>
				</ul>
			</p>
		</div>
		<div class="card-footer">
			<p>Communications &amp; Marketing</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				CSTEP program received a four-year grant of $70,000 per year to support student programming, program activities, and advisor staffing
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<p class="card-text">
				Conducted and Institutional Readiness for online delivery
			</p>
		</div>	
	</div>
</div>