<h2>To promote regional, statewide and international partnerships</h2>

<!-- <p>
	In this area, we should possibly define partnerships in this context.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>6.1</b>
			</td>
			<td>
				Leverage advisory boards to provide input on curriculum development and workforce partnerships
			</td>
		</tr>
		<tr>
			<td>
				<b>6.2</b>
			</td>
			<td>
				Increase and align educational offerings and workforce development training to meet community needs
			</td>
		</tr>
		<tr>
			<td>
				<b>6.3</b>
			</td>
			<td>
				Develop a network of sustainable partnerships with external stakeholders to serve as a catalyst for economic development and growth
			</td>
		</tr>
		<tr>
			<td>
				<b>6.4</b>
			</td>
			<td>
				Establish partnerships to promote campus internationalization and global outreach
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<ul class="list-group priorities">
	<li class="list-group-item">Partnerships: Hire the PIF-funded College Completion Specialist; Begin enhancing transfer (in) pathways; Explore degree completion options at MVCC, OCC and BCC </li>
	<li class="list-group-item">Serve on SUNY Student Conduct Institute Steering Committee as a rep for the STIXCA and SUNYJA organizations</li>
</ul>

<h3>Recent Accomplishments</h3>
<div class="card-columns mb-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Plantagon International</h4>
			<p class="card-text">
				Plantagon International toured the CEA facility and presented to faculty, staff and students
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Green Empire Farms, Inc.</h4>
			<p class="card-text">
				Collaborated with Green Empire Farms, Inc.
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">SUNY Admin. State Operating Funds Working Group</h4>
			<p class="card-text">
				Accounting faculty participated in the SUNY Admin. State Operating Funds Working Group solution
			</p>
		</div>	
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Colgate University</h4>
			<p class="card-text">
				Students and staff attending the Spectrum conference at Colgate University focused on issues and ideas to support LGBTQ communities on our campuses
			</p>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Commerce Chenango</h4>
			<p class="card-text">
				Worked with Commerce Chenango to host the second annual College and Community Job Fair at the Norwich Campus in November 2017, which attracted 274 students and community members
			</p>
		</div>	
	</div>
	<div class="card">
		<img class="card-img" src="img/accomplishments/solar-03.png" alt="working on solar panel">
	</div>	
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Family Enrichment Network</h4>
			<p class="card-text">
				The Early Childhood Program has co-hosted the local Early Childhood Conference, with the Family Enrichment Network, for the past 10 years
			</p>
		</div>	
	</div>

</div>