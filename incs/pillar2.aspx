<div class="tab-pane fade show mt-4" id="pillar-02" role="tabpanel" aria-labelledby="pillar-02-tab">
	<div class="row d-flex flex-row-reverse">
		<div class="col-lg-3 mb-4">
			<div class="nav flex-column nav-pills sticky-top" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-goal-04-tab" data-toggle="pill" href="#v-pills-goal-04" role="tab" aria-controls="v-pills-goal-04" aria-selected="true">
					<b>Goal 4:</b> To create a vibrant campus community for personal interaction and growth
				</a>
				<a class="nav-link" id="v-pills-goal-05-tab" data-toggle="pill" href="#v-pills-goal-05" role="tab" aria-controls="v-pills-goal-05" aria-selected="false">
					<b>Goal 5:</b> To engage the local community in civic and cultural affairs
				</a>
				<a class="nav-link" id="v-pills-goal-06-tab" data-toggle="pill" href="#v-pills-goal-06" role="tab" aria-controls="v-pills-goal-06" aria-selected="false">
					<b>Goal 6:</b> To promote regional, statewide and international partnerships
				</a>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-goal-04" role="tabpanel" aria-labelledby="v-pills-goal-04-tab">
					<!-- #include file= "pillar2-goal4.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-05" role="tabpanel" aria-labelledby="v-pills-goal-05-tab">
					<!-- #include file= "pillar2-goal5.aspx" -->
				</div>
				<div class="tab-pane fade" id="v-pills-goal-06" role="tabpanel" aria-labelledby="v-pills-goal-06-tab">
					<!-- #include file= "pillar2-goal6.aspx" -->
				</div>
			</div>
		</div>
	</div>
</div>