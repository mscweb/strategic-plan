<h2>To offer career-focused, experiential learning</h2>

<!-- <p>
	In this area, we should possibly define experiential learning.
</p> -->

<h3>Strategies</h3>
<table class="table table-hover">
	<thead class="sr-only">
		<tr>
			<th>Strategy</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<b>1.1</b>
			</td>
			<td>Provide quality instruction that meets student, community and employer expectations and standards</td>
		</tr>
		<tr>
			<td>
				<b>1.2</b>
			</td>
			<td>Provide on-and off-campus experiential learning opportunities</td>
		</tr>
		<tr>
			<td>
				<b>1.3</b>
			</td>
			<td>
				Create service-learning opportunities to enrich student involvement to support community-based initiatives
			</td>
		</tr>
	</tbody>
</table>

<h3>Future Priorities</h3>
<div class="accordion mb-4" id="accordion">
	<div class="card">
		<div class="card-header collapsed" id="heading-01" data-toggle="collapse" data-target="#collapse-01" aria-expanded="true" aria-controls="collapse-01">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					SUNY Morrisville Performance Improvement Fund Initiatives
				</button>
			</h4>
		</div>

		<div id="collapse-01" class="collapse" aria-labelledby="heading-01" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Plan and develop the College's first graduate program: an M.S. in Agribusiness</li>
					<li class="list-group-item">Host student showcase in applied learning to demonstrate alignment to high impact learning practices</li>
					<li class="list-group-item">Deliver industry workforce training in manufacturing</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-02" data-toggle="collapse" data-target="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Academic Program and Course Development
				</button>
			</h4>
		</div>
		<div id="collapse-02" class="collapse" aria-labelledby="heading-02" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Review the feasibility of an Animal Science BS degree</li>
					<li class="list-group-item">Announce the B.S. degree in Brewing Science & Technology and Craft Brewing Minor</li>
					<li class="list-group-item">Explore health-related programs (OTA and PTA)</li>
					<li class="list-group-item">Expand online and hybrid course offerings with instructional design support following recommendations from the SUNY Institutional Readiness Report</li>
					<li class="list-group-item">Increase early college, college in the high school, community/corporate-based courses</li>
					<li class="list-group-item">Establish new workforce development programs in Phlebotomy and Barbering through Educational Opportunity Center (EOC)</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-03" data-toggle="collapse" data-target="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Morrisville Campus Academic Programs Under Development
				</button>
			</h4>
		</div>
		<div id="collapse-03" class="collapse" aria-labelledby="heading-03" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Agricultural Science B.S. and B.Tech</li>
					<li class="list-group-item">Animal Science B.S.</li>
					<li class="list-group-item">Sustainable Resource Management B.S. (1-4)</li>
					<li class="list-group-item">Diesel Technology or Agricultural Engineering B.Tech (1-4)</li>
					<li class="list-group-item">Brewing Certificate and/or bachelor's degree in Brewing Science & Technology</li>
					<li class="list-group-item">Automotive Body Certificate</li>
					<li class="list-group-item">Certified Dietary Manager Certificate</li>
					<li class="list-group-item">General Design A.A.S.</li>
					<li class="list-group-item">Residential Construction A.A.S.</li>
					<li class="list-group-item">Dietetic Technician Bachelor's degree</li>
					<li class="list-group-item">Applied Biosciences B.Tech. (1-4)</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-04" data-toggle="collapse" data-target="#collapse-04" aria-expanded="false" aria-controls="collapse-04">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Norwich Academic Programs under development
				</button>
			</h4>
		</div>
		<div id="collapse-04" class="collapse" aria-labelledby="heading-04" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Integrative Science Certificate</li>
					<li class="list-group-item">Advanced Biology Teaching Certificate</li>
					<li class="list-group-item">Mechanical Engineering Technology Certificate</li>
					<li class="list-group-item">Biology A.S.</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-05" data-toggle="collapse" data-target="#collapse-05" aria-expanded="false" aria-controls="collapse-05">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Agriculture & Natural Resources
				</button>
			</h4>
		</div>
		<div id="collapse-05" class="collapse" aria-labelledby="heading-05" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Collaborate with Residential Construction and Wood Technology programs to integrate coursework and transfer them into the Agricultural Engineering Department</li>
					<li class="list-group-item">Develop new coursework in Precision Farming and Farm Machinery</li>
					<li class="list-group-item">Work with Psychology Department to explore the possibility of minors in Horticulture and Psychology</li>
					<li class="list-group-item">Have Village of Morrisville designated Tree City USA by Spring 2019</li>
					<li class="list-group-item">Continue efforts in maintaining SUNY Morrisville's Tree Campus USA status</li>
					<li class="list-group-item">Develop program to offer students two industry certifications: Interlocking Concrete Paver Institute; and National Concrete Masonry Association</li>
					<li class="list-group-item">Develop a new course in Coral Ecology</li>
					<li class="list-group-item">Acquire a grant for the production of sturgeon species for caviar</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-06" data-toggle="collapse" data-target="#collapse-06" aria-expanded="false" aria-controls="collapse-06">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Liberal Arts
				</button>
			</h4>
		</div>
		<div id="collapse-06" class="collapse" aria-labelledby="heading-06" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Introduce 3D Scanning Technology to crime scene investigation in Criminal Justice program</li>
					<li class="list-group-item">Develop a writing center</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header collapsed" id="heading-07" data-toggle="collapse" data-target="#collapse-07" aria-expanded="false" aria-controls="collapse-07">
			<h4 class="mb-0">
				<button class="btn btn-link" type="button">
					Science, Technology & Health Studies (STHS)
				</button>
			</h4>
		</div>
		<div id="collapse-07" class="collapse" aria-labelledby="heading-07" data-parent="#accordion">
			<div class="card-body">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Develop and receive SUNY and state approval for proposed programs in Math A.S. and Biology B.Tech, as well as the revitalization of degree offerings in CIT and full development of the MET workforce development certification program.</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header collapsed" id="heading-08" data-toggle="collapse" data-target="#collapse-08" aria-expanded="false" aria-controls="collapse-08">
				<h4 class="mb-0">
					<button class="btn btn-link" type="button">
						Educational Opportunity Center (EOC)
					</button>
				</h4>
			</div>
			<div id="collapse-08" class="collapse" aria-labelledby="heading-08" data-parent="#accordion">
				<div class="card-body">
					<ul class="list-group list-group-flush">
						<li class="list-group-item">Increase new and stackable certifications within vocational programs</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<h3>Recent Accomplishments</h3>
	<div class="card-columns mb-4">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Hosted First Applied Learning Summit</h4>
				<p class="card-text">"Sustainability and Growth in the 21<sup>st</sup> Century Workplace to Ensure New York Econimic Growth"</p>
			</div>
			<div class="card-footer"><p>2017</p></div>
		</div>
		<div class="card">
			<img class="card-img-top" src="img/accomplishments/agriculture-02.png" alt="agriculture students">
			<div class="card-body">
				<p class="card-text">
					Agriculture students:
					<ul>
						<li>participated in a nine-week predator monitoring study</li>
						<li>conducted woody invasive plant removals with NYS Parks at Clark Reservation</li>
						<li>worked with Soil and Water Conservation District to provide NYSDEC Endorsed 4-hour Erosion and Sediment Control Training and participate in streambank restoration</li>
					</ul>
				</p>
			</div>			
		</div>
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Master of Science in Agribusiness</h4>			
			</div>
			<div class="card-footer"><p>Letter of Intent Approved July 2018</p></div>
		</div>
		
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Human Services Certificate - Norwich Campus</h4>
			</div>
			<div class="card-footer"><p>Registered April 2018</p></div>
		</div>
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Nursing A.A.S. - <br />Norwich Campus</h4>	
			</div>
			<div class="card-footer"><p>Registered July 2018</p></div>
		</div>
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Landscape Design & Management A.S.</h4>
			</div>
			<div class="card-footer"><p>Registered 2018</p></div>
		</div>
		<div class="card featured">
			<div class="card-body">
				<h4 class="card-title">Brewing Science & Technology B.S.</h4>
			</div>
			<div class="card-footer"><p>Announced August 2018</p></div>
		</div>
		<div class="card">
			<img class="card-img-top" src="img/accomplishments/brewing-01.png" alt="brewing equipment">
			<div class="card-body">
				<h4 class="card-title">Fully-functioning Brewery at Copper Turret</h4>
				<p class="card-text"><b>Pending approval:</b> a minor in Brewing Science</p>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<p class="card-text">
					Students and faculty managed the College Farm and CEA Greenhouse produce production, harvest and processing a which supplies Seneca Dining Hall, The Copper Turret, and the campus community
				</p>
			</div>
		</div>
		<div class="card text-center p-3">
			<h4 class="card-title">Minors</h4>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Criminal Justice</li>
				<li class="list-group-item">Game Programming</li>
				<li class="list-group-item">History</li>
				<li class="list-group-item">Psychology</li>
				<li class="list-group-item">Science, Technology & Society</li>
			</ul>
		</div>

		<div class="card">			
			<div class="card-body">
				<p class="card-text">
					Criminal Justice students:
					<ul>
						<li>participated in Active Shooter Simulation with Chenango Co. Sheriff's Dept., Norwich PD, and University Police</li>
						<li>participated in Narcan training for Criminal Justice class</li>
						<li>shadowed security at Darien lake park and concert venue</li>
					</ul>
				</p>
			</div>
			<img class="card-img-bottom" src="img/accomplishments/crim-justice-01.png" alt="criminal justice students">
		</div>

		<div class="card">
			<div class="card-body">
				<p class="card-text">
					Students worked with Soil and Water Conservation District by visiting stream channel restoration sites and participated in streambank restoration
				</p>
			</div>
		</div>

	</div>